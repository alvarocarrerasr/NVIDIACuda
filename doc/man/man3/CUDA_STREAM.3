.TH "Stream Management" 3 "12 Jan 2017" "Version 6.0" "Doxygen" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Stream Management \- 
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamAddCallback\fP (\fBCUstream\fP hStream, \fBCUstreamCallback\fP callback, void *userData, unsigned int flags)"
.br
.RI "\fIAdd a callback to a compute stream. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamAttachMemAsync\fP (\fBCUstream\fP hStream, \fBCUdeviceptr\fP dptr, size_t length, unsigned int flags)"
.br
.RI "\fIAttach memory to a stream asynchronously. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamCreate\fP (\fBCUstream\fP *phStream, unsigned int Flags)"
.br
.RI "\fICreate a stream. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamCreateWithPriority\fP (\fBCUstream\fP *phStream, unsigned int flags, int priority)"
.br
.RI "\fICreate a stream with the given priority. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamDestroy\fP (\fBCUstream\fP hStream)"
.br
.RI "\fIDestroys a stream. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamGetFlags\fP (\fBCUstream\fP hStream, unsigned int *flags)"
.br
.RI "\fIQuery the flags of a given stream. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamGetPriority\fP (\fBCUstream\fP hStream, int *priority)"
.br
.RI "\fIQuery the priority of a given stream. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamQuery\fP (\fBCUstream\fP hStream)"
.br
.RI "\fIDetermine status of a compute stream. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamSynchronize\fP (\fBCUstream\fP hStream)"
.br
.RI "\fIWait until a stream's tasks are completed. \fP"
.ti -1c
.RI "\fBCUresult\fP \fBcuStreamWaitEvent\fP (\fBCUstream\fP hStream, \fBCUevent\fP hEvent, unsigned int Flags)"
.br
.RI "\fIMake a compute stream wait on an event. \fP"
.in -1c
.SH "Detailed Description"
.PP 
\\brief stream management functions of the low-level CUDA driver API (\fBcuda.h\fP)
.PP
This section describes the stream management functions of the low-level CUDA driver application programming interface. 
.SH "Function Documentation"
.PP 
.SS "\fBCUresult\fP cuStreamAddCallback (\fBCUstream\fP hStream, \fBCUstreamCallback\fP callback, void * userData, unsigned int flags)"
.PP
Adds a callback to be called on the host after all currently enqueued items in the stream have completed. For each cuStreamAddCallback call, the callback will be executed exactly once. The callback will block later work in the stream until it is finished.
.PP
The callback may be passed \fBCUDA_SUCCESS\fP or an error code. In the event of a device error, all subsequently executed callbacks will receive an appropriate \fBCUresult\fP.
.PP
Callbacks must not make any CUDA API calls. Attempting to use a CUDA API will result in \fBCUDA_ERROR_NOT_PERMITTED\fP. Callbacks must not perform any synchronization that may depend on outstanding device work or other callbacks that are not mandated to run earlier. Callbacks without a mandated order (in independent streams) execute in undefined order and may be serialized.
.PP
This API requires compute capability 1.1 or greater. See \fBcuDeviceGetAttribute\fP or \fBcuDeviceGetProperties\fP to query compute capability. Attempting to use this API with earlier compute versions will return \fBCUDA_ERROR_NOT_SUPPORTED\fP.
.PP
For the purposes of Unified Memory, callback execution makes a number of guarantees: 
.PD 0

.IP "\(bu" 2
The callback stream is considered idle for the duration of the callback. Thus, for example, a callback may always use memory attached to the callback stream. 
.IP "\(bu" 2
The start of execution of a callback has the same effect as synchronizing an event recorded in the same stream immediately prior to the callback. It thus synchronizes streams which have been 'joined' prior to the callback. 
.IP "\(bu" 2
Adding device work to any stream does not have the effect of making the stream active until all preceding callbacks have executed. Thus, for example, a callback might use global attached memory even if work has been added to another stream, if it has been properly ordered with an event. 
.IP "\(bu" 2
Completion of a callback does not cause a stream to become active except as described above. The callback stream will remain idle if no device work follows the callback, and will remain idle across consecutive callbacks without device work in between. Thus, for example, stream synchronization can be done by signaling from a callback at the end of the stream. 
.PP
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Stream to add callback to 
.br
\fIcallback\fP - The function to call once preceding stream operations are complete 
.br
\fIuserData\fP - User specified data to be passed to the callback function 
.br
\fIflags\fP - Reserved for future use, must be 0
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP, \fBCUDA_ERROR_NOT_SUPPORTED\fP 
.RE
.PP
\fBNote:\fP
.RS 4
This function uses standard  semantics. 
.PP
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamCreate\fP, \fBcuStreamQuery\fP, \fBcuStreamSynchronize\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamDestroy\fP, \fBcuMemAllocManaged\fP, \fBcuStreamAttachMemAsync\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamAttachMemAsync (\fBCUstream\fP hStream, \fBCUdeviceptr\fP dptr, size_t length, unsigned int flags)"
.PP
Enqueues an operation in \fChStream\fP to specify stream association of \fClength\fP bytes of memory starting from \fCdptr\fP. This function is a stream-ordered operation, meaning that it is dependent on, and will only take effect when, previous work in stream has completed. Any previous association is automatically replaced.
.PP
\fCdptr\fP must point to an address within managed memory space declared using the __managed__ keyword or allocated with \fBcuMemAllocManaged\fP.
.PP
\fClength\fP must be zero, to indicate that the entire allocation's stream association is being changed. Currently, it's not possible to change stream association for a portion of an allocation.
.PP
The stream association is specified using \fCflags\fP which must be one of \fBCUmemAttach_flags\fP. If the \fBCU_MEM_ATTACH_GLOBAL\fP flag is specified, the memory can be accessed by any stream on any device. If the \fBCU_MEM_ATTACH_HOST\fP flag is specified, the program makes a guarantee that it won't access the memory on the device from any stream on a device that has a zero value for the device attribute \fBCU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS\fP. If the \fBCU_MEM_ATTACH_SINGLE\fP flag is specified and \fChStream\fP is associated with a device that has a zero value for the device attribute \fBCU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS\fP, the program makes a guarantee that it will only access the memory on the device from \fChStream\fP. It is illegal to attach singly to the NULL stream, because the NULL stream is a virtual global stream and not a specific stream. An error will be returned in this case.
.PP
When memory is associated with a single stream, the Unified Memory system will allow CPU access to this memory region so long as all operations in \fChStream\fP have completed, regardless of whether other streams are active. In effect, this constrains exclusive ownership of the managed memory region by an active GPU to per-stream activity instead of whole-GPU activity.
.PP
Accessing memory on the device from streams that are not associated with it will produce undefined results. No error checking is performed by the Unified Memory system to ensure that kernels launched into other streams do not access this region.
.PP
It is a program's responsibility to order calls to \fBcuStreamAttachMemAsync\fP via events, synchronization or other means to ensure legal access to memory at all times. Data visibility and coherency will be changed appropriately for all kernels which follow a stream-association change.
.PP
If \fChStream\fP is destroyed while data is associated with it, the association is removed and the association reverts to the default visibility of the allocation as specified at \fBcuMemAllocManaged\fP. For __managed__ variables, the default association is always \fBCU_MEM_ATTACH_GLOBAL\fP. Note that destroying a stream is an asynchronous operation, and as a result, the change to default association won't happen until all work in the stream has completed.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Stream in which to enqueue the attach operation 
.br
\fIdptr\fP - Pointer to memory (must be a pointer to managed memory) 
.br
\fIlength\fP - Length of memory (must be zero) 
.br
\fIflags\fP - Must be one of \fBCUmemAttach_flags\fP
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP, \fBCUDA_ERROR_NOT_SUPPORTED\fP 
.RE
.PP
\fBNote:\fP
.RS 4
This function uses standard  semantics. 
.PP
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamCreate\fP, \fBcuStreamQuery\fP, \fBcuStreamSynchronize\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamDestroy\fP, \fBcuMemAllocManaged\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamCreate (\fBCUstream\fP * phStream, unsigned int Flags)"
.PP
Creates a stream and returns a handle in \fCphStream\fP. The \fCFlags\fP argument determines behaviors of the stream. Valid values for \fCFlags\fP are:
.IP "\(bu" 2
\fBCU_STREAM_DEFAULT\fP: Default stream creation flag.
.IP "\(bu" 2
\fBCU_STREAM_NON_BLOCKING\fP: Specifies that work running in the created stream may run concurrently with work in stream 0 (the NULL stream), and that the created stream should perform no implicit synchronization with stream 0.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIphStream\fP - Returned newly created stream 
.br
\fIFlags\fP - Parameters for stream creation
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_VALUE\fP, \fBCUDA_ERROR_OUT_OF_MEMORY\fP 
.RE
.PP
\fBNote:\fP
.RS 4
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamDestroy\fP, \fBcuStreamCreateWithPriority\fP, \fBcuStreamGetPriority\fP, \fBcuStreamGetFlags\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamQuery\fP, \fBcuStreamSynchronize\fP, \fBcuStreamAddCallback\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamCreateWithPriority (\fBCUstream\fP * phStream, unsigned int flags, int priority)"
.PP
Creates a stream with the specified priority and returns a handle in \fCphStream\fP. This API alters the scheduler priority of work in the stream. Work in a higher priority stream may preempt work already executing in a low priority stream.
.PP
\fCpriority\fP follows a convention where lower numbers represent higher priorities. '0' represents default priority. The range of meaningful numerical priorities can be queried using \fBcuCtxGetStreamPriorityRange\fP. If the specified priority is outside the numerical range returned by \fBcuCtxGetStreamPriorityRange\fP, it will automatically be clamped to the lowest or the highest number in the range.
.PP
\fBParameters:\fP
.RS 4
\fIphStream\fP - Returned newly created stream 
.br
\fIflags\fP - Flags for stream creation. See \fBcuStreamCreate\fP for a list of valid flags 
.br
\fIpriority\fP - Stream priority. Lower numbers represent higher priorities. See \fBcuCtxGetStreamPriorityRange\fP for more information about meaningful stream priorities that can be passed.
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_VALUE\fP, \fBCUDA_ERROR_OUT_OF_MEMORY\fP 
.RE
.PP
\fBNote:\fP
.RS 4
Note that this function may also return error codes from previous, asynchronous launches.
.PP
Stream priorities are supported only on Quadro and Tesla GPUs with compute capability 3.5 or higher.
.PP
In the current implementation, only compute kernels launched in priority streams are affected by the stream's priority. Stream priorities have no effect on host-to-device and device-to-host memory operations.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamDestroy\fP, \fBcuStreamCreate\fP, \fBcuStreamGetPriority\fP, \fBcuCtxGetStreamPriorityRange\fP, \fBcuStreamGetFlags\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamQuery\fP, \fBcuStreamSynchronize\fP, \fBcuStreamAddCallback\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamDestroy (\fBCUstream\fP hStream)"
.PP
Destroys the stream specified by \fChStream\fP.
.PP
In case the device is still doing work in the stream \fChStream\fP when \fBcuStreamDestroy()\fP is called, the function will return immediately and the resources associated with \fChStream\fP will be released automatically once the device has completed all work in \fChStream\fP.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Stream to destroy
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_VALUE\fP 
.RE
.PP
\fBNote:\fP
.RS 4
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamCreate\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamQuery\fP, \fBcuStreamSynchronize\fP, \fBcuStreamAddCallback\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamGetFlags (\fBCUstream\fP hStream, unsigned int * flags)"
.PP
Query the flags of a stream created using \fBcuStreamCreate\fP or \fBcuStreamCreateWithPriority\fP and return the flags in \fCflags\fP.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Handle to the stream to be queried 
.br
\fIflags\fP - Pointer to an unsigned integer in which the stream's flags are returned The value returned in \fCflags\fP is a logical 'OR' of all flags that were used while creating this stream. See \fBcuStreamCreate\fP for the list of valid flags 
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_VALUE\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP, \fBCUDA_ERROR_OUT_OF_MEMORY\fP 
.RE
.PP
\fBNote:\fP
.RS 4
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamDestroy\fP, \fBcuStreamCreate\fP, \fBcuStreamGetPriority\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamGetPriority (\fBCUstream\fP hStream, int * priority)"
.PP
Query the priority of a stream created using \fBcuStreamCreate\fP or \fBcuStreamCreateWithPriority\fP and return the priority in \fCpriority\fP. Note that if the stream was created with a priority outside the numerical range returned by \fBcuCtxGetStreamPriorityRange\fP, this function returns the clamped priority. See \fBcuStreamCreateWithPriority\fP for details about priority clamping.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Handle to the stream to be queried 
.br
\fIpriority\fP - Pointer to a signed integer in which the stream's priority is returned 
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_VALUE\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP, \fBCUDA_ERROR_OUT_OF_MEMORY\fP 
.RE
.PP
\fBNote:\fP
.RS 4
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamDestroy\fP, \fBcuStreamCreate\fP, \fBcuStreamCreateWithPriority\fP, \fBcuCtxGetStreamPriorityRange\fP, \fBcuStreamGetFlags\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamQuery (\fBCUstream\fP hStream)"
.PP
Returns \fBCUDA_SUCCESS\fP if all operations in the stream specified by \fChStream\fP have completed, or \fBCUDA_ERROR_NOT_READY\fP if not.
.PP
For the purposes of Unified Memory, a return value of \fBCUDA_SUCCESS\fP is equivalent to having called \fBcuStreamSynchronize()\fP.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Stream to query status of
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP, \fBCUDA_ERROR_NOT_READY\fP 
.RE
.PP
\fBNote:\fP
.RS 4
This function uses standard  semantics. 
.PP
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamCreate\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamDestroy\fP, \fBcuStreamSynchronize\fP, \fBcuStreamAddCallback\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamSynchronize (\fBCUstream\fP hStream)"
.PP
Waits until the device has completed all operations in the stream specified by \fChStream\fP. If the context was created with the \fBCU_CTX_SCHED_BLOCKING_SYNC\fP flag, the CPU thread will block until the stream is finished with all of its tasks.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Stream to wait for
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP 
.RE
.PP
\fBNote:\fP
.RS 4
This function uses standard  semantics. 
.PP
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamCreate\fP, \fBcuStreamDestroy\fP, \fBcuStreamWaitEvent\fP, \fBcuStreamQuery\fP, \fBcuStreamAddCallback\fP 
.RE
.PP

.SS "\fBCUresult\fP cuStreamWaitEvent (\fBCUstream\fP hStream, \fBCUevent\fP hEvent, unsigned int Flags)"
.PP
Makes all future work submitted to \fChStream\fP wait until \fChEvent\fP reports completion before beginning execution. This synchronization will be performed efficiently on the device. The event \fChEvent\fP may be from a different context than \fChStream\fP, in which case this function will perform cross-device synchronization.
.PP
The stream \fChStream\fP will wait only for the completion of the most recent host call to \fBcuEventRecord()\fP on \fChEvent\fP. Once this call has returned, any functions (including \fBcuEventRecord()\fP and \fBcuEventDestroy()\fP) may be called on \fChEvent\fP again, and subsequent calls will not have any effect on \fChStream\fP.
.PP
If \fBcuEventRecord()\fP has not been called on \fChEvent\fP, this call acts as if the record has already completed, and so is a functional no-op.
.PP
\fBParameters:\fP
.RS 4
\fIhStream\fP - Stream to wait 
.br
\fIhEvent\fP - Event to wait on (may not be NULL) 
.br
\fIFlags\fP - Parameters for the operation (must be 0)
.RE
.PP
\fBReturns:\fP
.RS 4
\fBCUDA_SUCCESS\fP, \fBCUDA_ERROR_DEINITIALIZED\fP, \fBCUDA_ERROR_NOT_INITIALIZED\fP, \fBCUDA_ERROR_INVALID_CONTEXT\fP, \fBCUDA_ERROR_INVALID_HANDLE\fP, 
.RE
.PP
\fBNote:\fP
.RS 4
This function uses standard  semantics. 
.PP
Note that this function may also return error codes from previous, asynchronous launches.
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcuStreamCreate\fP, \fBcuEventRecord\fP, \fBcuStreamQuery\fP, \fBcuStreamSynchronize\fP, \fBcuStreamAddCallback\fP, \fBcuStreamDestroy\fP 
.RE
.PP

.SH "Author"
.PP 
Generated automatically by Doxygen from the source code.
