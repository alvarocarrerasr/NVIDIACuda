.TH "Texture Object Management" 3 "12 Jan 2017" "Version 6.0" "Doxygen" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Texture Object Management \- 
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBcudaError_t\fP \fBcudaCreateTextureObject\fP (\fBcudaTextureObject_t\fP *pTexObject, const struct \fBcudaResourceDesc\fP *pResDesc, const struct \fBcudaTextureDesc\fP *pTexDesc, const struct \fBcudaResourceViewDesc\fP *pResViewDesc)"
.br
.RI "\fICreates a texture object. \fP"
.ti -1c
.RI "\fBcudaError_t\fP \fBcudaDestroyTextureObject\fP (\fBcudaTextureObject_t\fP texObject)"
.br
.RI "\fIDestroys a texture object. \fP"
.ti -1c
.RI "\fBcudaError_t\fP \fBcudaGetTextureObjectResourceDesc\fP (struct \fBcudaResourceDesc\fP *pResDesc, \fBcudaTextureObject_t\fP texObject)"
.br
.RI "\fIReturns a texture object's resource descriptor. \fP"
.ti -1c
.RI "\fBcudaError_t\fP \fBcudaGetTextureObjectResourceViewDesc\fP (struct \fBcudaResourceViewDesc\fP *pResViewDesc, \fBcudaTextureObject_t\fP texObject)"
.br
.RI "\fIReturns a texture object's resource view descriptor. \fP"
.ti -1c
.RI "\fBcudaError_t\fP \fBcudaGetTextureObjectTextureDesc\fP (struct \fBcudaTextureDesc\fP *pTexDesc, \fBcudaTextureObject_t\fP texObject)"
.br
.RI "\fIReturns a texture object's texture descriptor. \fP"
.in -1c
.SH "Detailed Description"
.PP 
\\brief texture object management functions of the CUDA runtime API (cuda_runtime_api.h)
.PP
This section describes the low level texture object management functions of the CUDA runtime application programming interface. The texture object API is only supported on devices of compute capability 3.0 or higher. 
.SH "Function Documentation"
.PP 
.SS "\fBcudaError_t\fP cudaCreateTextureObject (\fBcudaTextureObject_t\fP * pTexObject, const struct \fBcudaResourceDesc\fP * pResDesc, const struct \fBcudaTextureDesc\fP * pTexDesc, const struct \fBcudaResourceViewDesc\fP * pResViewDesc)"
.PP
Creates a texture object and returns it in \fCpTexObject\fP. \fCpResDesc\fP describes the data to texture from. \fCpTexDesc\fP describes how the data should be sampled. \fCpResViewDesc\fP is an optional argument that specifies an alternate format for the data described by \fCpResDesc\fP, and also describes the subresource region to restrict access to when texturing. \fCpResViewDesc\fP can only be specified if the type of resource is a CUDA array or a CUDA mipmapped array.
.PP
Texture objects are only supported on devices of compute capability 3.0 or higher. Additionally, a texture object is an opaque value, and, as such, should only be accessed through CUDA API calls.
.PP
The \fBcudaResourceDesc\fP structure is defined as: 
.PP
.nf
        struct cudaResourceDesc {
            enum cudaResourceType resType;
            
            union {
                struct {
                    cudaArray_t array;
                } array;
                struct {
                    cudaMipmappedArray_t mipmap;
                } mipmap;
                struct {
                    void *devPtr;
                    struct cudaChannelFormatDesc desc;
                    size_t sizeInBytes;
                } linear;
                struct {
                    void *devPtr;
                    struct cudaChannelFormatDesc desc;
                    size_t width;
                    size_t height;
                    size_t pitchInBytes;
                } pitch2D;
            } res;
        };

.fi
.PP
 where:
.IP "\(bu" 2
\fBcudaResourceDesc::resType\fP specifies the type of resource to texture from. CUresourceType is defined as: 
.PP
.nf
        enum cudaResourceType {
            cudaResourceTypeArray          = 0x00,
            cudaResourceTypeMipmappedArray = 0x01,
            cudaResourceTypeLinear         = 0x02,
            cudaResourceTypePitch2D        = 0x03
        };

.fi
.PP

.PP
.PP
\fB\fP.RS 4
If \fBcudaResourceDesc::resType\fP is set to \fBcudaResourceTypeArray\fP, cudaResourceDesc::res::array::array must be set to a valid CUDA array handle.
.RE
.PP
\fB\fP.RS 4
If \fBcudaResourceDesc::resType\fP is set to \fBcudaResourceTypeMipmappedArray\fP, cudaResourceDesc::res::mipmap::mipmap must be set to a valid CUDA mipmapped array handle and \fBcudaTextureDesc::normalizedCoords\fP must be set to true.
.RE
.PP
\fB\fP.RS 4
If \fBcudaResourceDesc::resType\fP is set to \fBcudaResourceTypeLinear\fP, cudaResourceDesc::res::linear::devPtr must be set to a valid device pointer, that is aligned to \fBcudaDeviceProp::textureAlignment\fP. cudaResourceDesc::res::linear::desc describes the format and the number of components per array element. cudaResourceDesc::res::linear::sizeInBytes specifies the size of the array in bytes. The total number of elements in the linear address range cannot exceed \fBcudaDeviceProp::maxTexture1DLinear\fP. The number of elements is computed as (sizeInBytes / sizeof(desc)).
.RE
.PP
\fB\fP.RS 4
If \fBcudaResourceDesc::resType\fP is set to \fBcudaResourceTypePitch2D\fP, cudaResourceDesc::res::pitch2D::devPtr must be set to a valid device pointer, that is aligned to \fBcudaDeviceProp::textureAlignment\fP. cudaResourceDesc::res::pitch2D::desc describes the format and the number of components per array element. cudaResourceDesc::res::pitch2D::width and cudaResourceDesc::res::pitch2D::height specify the width and height of the array in elements, and cannot exceed \fBcudaDeviceProp::maxTexture2DLinear\fP[0] and \fBcudaDeviceProp::maxTexture2DLinear\fP[1] respectively. cudaResourceDesc::res::pitch2D::pitchInBytes specifies the pitch between two rows in bytes and has to be aligned to \fBcudaDeviceProp::texturePitchAlignment\fP. Pitch cannot exceed \fBcudaDeviceProp::maxTexture2DLinear\fP[2].
.RE
.PP
The \fBcudaTextureDesc\fP struct is defined as 
.PP
.nf
        struct cudaTextureDesc {
            enum cudaTextureAddressMode addressMode[3];
            enum cudaTextureFilterMode  filterMode;
            enum cudaTextureReadMode    readMode;
            int                         sRGB;
            float                       borderColor[4];
            int                         normalizedCoords;
            unsigned int                maxAnisotropy;
            enum cudaTextureFilterMode  mipmapFilterMode;
            float                       mipmapLevelBias;
            float                       minMipmapLevelClamp;
            float                       maxMipmapLevelClamp;
        };

.fi
.PP
 where
.IP "\(bu" 2
\fBcudaTextureDesc::addressMode\fP specifies the addressing mode for each dimension of the texture data. \fBcudaTextureAddressMode\fP is defined as: 
.PP
.nf
        enum cudaTextureAddressMode {
            cudaAddressModeWrap   = 0,
            cudaAddressModeClamp  = 1,
            cudaAddressModeMirror = 2,
            cudaAddressModeBorder = 3
        };

.fi
.PP
 This is ignored if \fBcudaResourceDesc::resType\fP is \fBcudaResourceTypeLinear\fP. Also, if \fBcudaTextureDesc::normalizedCoords\fP is set to zero, \fBcudaAddressModeWrap\fP and \fBcudaAddressModeMirror\fP won't be supported and will be switched to \fBcudaAddressModeClamp\fP.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::filterMode\fP specifies the filtering mode to be used when fetching from the texture. \fBcudaTextureFilterMode\fP is defined as: 
.PP
.nf
        enum cudaTextureFilterMode {
            cudaFilterModePoint  = 0,
            cudaFilterModeLinear = 1
        };

.fi
.PP
 This is ignored if \fBcudaResourceDesc::resType\fP is \fBcudaResourceTypeLinear\fP.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::readMode\fP specifies whether integer data should be converted to floating point or not. \fBcudaTextureReadMode\fP is defined as: 
.PP
.nf
        enum cudaTextureReadMode {
            cudaReadModeElementType     = 0,
            cudaReadModeNormalizedFloat = 1
        };

.fi
.PP
 Note that this applies only to 8-bit and 16-bit integer formats. 32-bit integer format would not be promoted, regardless of whether or not this \fBcudaTextureDesc::readMode\fP is set \fBcudaReadModeNormalizedFloat\fP is specified.
.PP
.PP
.IP "\(bu" 2
cudaTextureDesc::sRGB specifies whether sRGB to linear conversion should be performed during texture fetch.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::borderColor\fP specifies the float values of color. where: \fBcudaTextureDesc::borderColor\fP[0] contains value of 'R', \fBcudaTextureDesc::borderColor\fP[1] contains value of 'G', \fBcudaTextureDesc::borderColor\fP[2] contains value of 'B', \fBcudaTextureDesc::borderColor\fP[3] contains value of 'A' Note that application using integer border color values will need to <reinterpret_cast> these values to float. The values are set only when the addressing mode specified by \fBcudaTextureDesc::addressMode\fP is cudaAddressModeBorder.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::normalizedCoords\fP specifies whether the texture coordinates will be normalized or not.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::maxAnisotropy\fP specifies the maximum anistropy ratio to be used when doing anisotropic filtering. This value will be clamped to the range [1,16].
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::mipmapFilterMode\fP specifies the filter mode when the calculated mipmap level lies between two defined mipmap levels.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::mipmapLevelBias\fP specifies the offset to be applied to the calculated mipmap level.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::minMipmapLevelClamp\fP specifies the lower end of the mipmap level range to clamp access to.
.PP
.PP
.IP "\(bu" 2
\fBcudaTextureDesc::maxMipmapLevelClamp\fP specifies the upper end of the mipmap level range to clamp access to.
.PP
.PP
The \fBcudaResourceViewDesc\fP struct is defined as 
.PP
.nf
        struct cudaResourceViewDesc {
            enum cudaResourceViewFormat format;
            size_t                      width;
            size_t                      height;
            size_t                      depth;
            unsigned int                firstMipmapLevel;
            unsigned int                lastMipmapLevel;
            unsigned int                firstLayer;
            unsigned int                lastLayer;
        };

.fi
.PP
 where:
.IP "\(bu" 2
cudaResourceViewDesc::format specifies how the data contained in the CUDA array or CUDA mipmapped array should be interpreted. Note that this can incur a change in size of the texture data. If the resource view format is a block compressed format, then the underlying CUDA array or CUDA mipmapped array has to have a 32-bit unsigned integer format with 2 or 4 channels, depending on the block compressed format. For ex., BC1 and BC4 require the underlying CUDA array to have a 32-bit unsigned int with 2 channels. The other BC formats require the underlying resource to have the same 32-bit unsigned int format but with 4 channels.
.PP
.PP
.IP "\(bu" 2
cudaResourceViewDesc::width specifies the new width of the texture data. If the resource view format is a block compressed format, this value has to be 4 times the original width of the resource. For non block compressed formats, this value has to be equal to that of the original resource.
.PP
.PP
.IP "\(bu" 2
cudaResourceViewDesc::height specifies the new height of the texture data. If the resource view format is a block compressed format, this value has to be 4 times the original height of the resource. For non block compressed formats, this value has to be equal to that of the original resource.
.PP
.PP
.IP "\(bu" 2
cudaResourceViewDesc::depth specifies the new depth of the texture data. This value has to be equal to that of the original resource.
.PP
.PP
.IP "\(bu" 2
\fBcudaResourceViewDesc::firstMipmapLevel\fP specifies the most detailed mipmap level. This will be the new mipmap level zero. For non-mipmapped resources, this value has to be zero.\fBcudaTextureDesc::minMipmapLevelClamp\fP and \fBcudaTextureDesc::maxMipmapLevelClamp\fP will be relative to this value. For ex., if the firstMipmapLevel is set to 2, and a minMipmapLevelClamp of 1.2 is specified, then the actual minimum mipmap level clamp will be 3.2.
.PP
.PP
.IP "\(bu" 2
\fBcudaResourceViewDesc::lastMipmapLevel\fP specifies the least detailed mipmap level. For non-mipmapped resources, this value has to be zero.
.PP
.PP
.IP "\(bu" 2
\fBcudaResourceViewDesc::firstLayer\fP specifies the first layer index for layered textures. This will be the new layer zero. For non-layered resources, this value has to be zero.
.PP
.PP
.IP "\(bu" 2
\fBcudaResourceViewDesc::lastLayer\fP specifies the last layer index for layered textures. For non-layered resources, this value has to be zero.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIpTexObject\fP - Texture object to create 
.br
\fIpResDesc\fP - Resource descriptor 
.br
\fIpTexDesc\fP - Texture descriptor 
.br
\fIpResViewDesc\fP - Resource view descriptor
.RE
.PP
\fBReturns:\fP
.RS 4
\fBcudaSuccess\fP, \fBcudaErrorInvalidValue\fP
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcudaDestroyTextureObject\fP 
.RE
.PP

.SS "\fBcudaError_t\fP cudaDestroyTextureObject (\fBcudaTextureObject_t\fP texObject)"
.PP
Destroys the texture object specified by \fCtexObject\fP.
.PP
\fBParameters:\fP
.RS 4
\fItexObject\fP - Texture object to destroy
.RE
.PP
\fBReturns:\fP
.RS 4
\fBcudaSuccess\fP, \fBcudaErrorInvalidValue\fP
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcudaCreateTextureObject\fP 
.RE
.PP

.SS "\fBcudaError_t\fP cudaGetTextureObjectResourceDesc (struct \fBcudaResourceDesc\fP * pResDesc, \fBcudaTextureObject_t\fP texObject)"
.PP
Returns the resource descriptor for the texture object specified by \fCtexObject\fP.
.PP
\fBParameters:\fP
.RS 4
\fIpResDesc\fP - Resource descriptor 
.br
\fItexObject\fP - Texture object
.RE
.PP
\fBReturns:\fP
.RS 4
\fBcudaSuccess\fP, \fBcudaErrorInvalidValue\fP
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcudaCreateTextureObject\fP 
.RE
.PP

.SS "\fBcudaError_t\fP cudaGetTextureObjectResourceViewDesc (struct \fBcudaResourceViewDesc\fP * pResViewDesc, \fBcudaTextureObject_t\fP texObject)"
.PP
Returns the resource view descriptor for the texture object specified by \fCtexObject\fP. If no resource view was specified, \fBcudaErrorInvalidValue\fP is returned.
.PP
\fBParameters:\fP
.RS 4
\fIpResViewDesc\fP - Resource view descriptor 
.br
\fItexObject\fP - Texture object
.RE
.PP
\fBReturns:\fP
.RS 4
\fBcudaSuccess\fP, \fBcudaErrorInvalidValue\fP
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcudaCreateTextureObject\fP 
.RE
.PP

.SS "\fBcudaError_t\fP cudaGetTextureObjectTextureDesc (struct \fBcudaTextureDesc\fP * pTexDesc, \fBcudaTextureObject_t\fP texObject)"
.PP
Returns the texture descriptor for the texture object specified by \fCtexObject\fP.
.PP
\fBParameters:\fP
.RS 4
\fIpTexDesc\fP - Texture descriptor 
.br
\fItexObject\fP - Texture object
.RE
.PP
\fBReturns:\fP
.RS 4
\fBcudaSuccess\fP, \fBcudaErrorInvalidValue\fP
.RE
.PP
\fBSee also:\fP
.RS 4
\fBcudaCreateTextureObject\fP 
.RE
.PP

.SH "Author"
.PP 
Generated automatically by Doxygen from the source code.
